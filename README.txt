This is my personal game engine I created from scratch using SDL and Tiled.
I read Game Coding Complete and created it roughly based on that game engine.  So some of the code is from the book.
The book does not use Tiled at all or SDL, which is what I've used.